package com.example.myapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.myapplication.data.ContactData
import com.example.myapplication.data.MainAdapter
import com.example.myapplication.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val listContact = arrayListOf(
            ContactData(1,R.drawable.ic_baseline_person_24, "Alex", "08134576"),
            ContactData(2,R.drawable.ic_baseline_person_24, "Ruben", "0912455949"),
            ContactData(3,R.drawable.ic_baseline_person_24, "Josua", "081767784")
        )

        //make adapter
        val adapter = MainAdapter(listContact)

        //make layout manager
        val layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        binding.listData.layoutManager = layoutManager
        binding.listData.adapter = adapter
    }



}