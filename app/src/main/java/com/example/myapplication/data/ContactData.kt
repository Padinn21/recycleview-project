package com.example.myapplication.data

data class ContactData(
    var id: Int,
    var photoId: Int,
    var name: String,
    var number: String
)