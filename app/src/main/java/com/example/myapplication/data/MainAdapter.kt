package com.example.myapplication.data
import android.view.*
import android.widget.ImageButton
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import android.widget.TextView
import com.example.myapplication.R

class MainAdapter(private val listContact: ArrayList<ContactData> )
    :RecyclerView.Adapter<MainAdapter.ViewHolder>() {


    // Holder Class
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val avatar: ImageView = itemView.findViewById(R.id.person_icon)
        val name = itemView.findViewById<TextView>(R.id.name)
        val number = itemView.findViewById<TextView>(R.id.number)
    }


    //make holder
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.contact_item, parent, false)
        return ViewHolder(view)
    }

    // melakukan penentuan data mana yang akan di tampilkan
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val data = listContact[position]
        holder.avatar.setImageResource(data.photoId)
        holder.name.text = data.name
        holder.number.text = data.number
    }

    //memberitahu banyak nya data
    override fun getItemCount(): Int {
        return listContact.size
    }





}